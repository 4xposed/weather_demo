# README

To run this app you need the following environment variables:

```
OPEN_WEATHER_API_KEY # https://home.openweathermap.org/api_keys
GOOGLE_API_KEY # https://developers.google.com/maps/documentation/javascript/places
```
