module IconForWeatherHelper
  ICON = {
    '01d' => 'wu-clear',
    '02d' => 'wu-partlysunny',
    '03d' => 'wu-cloudy',
    '04d' => 'wu-cloudy',
    '09d' => 'wu-rain',
    '10d' => 'wu-rain',
    '11d' => 'wu-tstorms',
    '13d' => 'wu-snow',
    '50d' => 'wu-fog'
  }
  DEFAULT_ICON = 'wu-unknown'
  CLASSES = ['wu', 'wu-white', 'wu-256']

  def icon_for(icon)
    icon_class = ICON.fetch(icon, DEFAULT_ICON)
    content_tag(:i, '', class: CLASSES + [icon_class])
  end
end
