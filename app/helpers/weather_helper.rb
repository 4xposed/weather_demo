module WeatherHelper
  def humidity_for(weather)
    content_tag(:li, %Q{ Humidity: #{weather.current.humidity} % })
  end

  def visibility_for(weather)
    content_tag(:li, %Q{ Visibility: #{weather.current.visibility} % })
  end

  def clouds_for(weather)
    content_tag(:li, %Q{ Clouds: #{weather.clouds} %})
  end

  def temperatures_for(weather)
    content_tag(:li, %Q{ Min. Temperature: #{weather.temperature.min} ºC }) +
    content_tag(:li, %Q{ Max. Temperature: #{weather.temperature.max} ºC })
  end

  def wind_for(weather)
    return '' if weather.wind.nil?
    content_tag(:li, %Q{ Wind speed: #{weather.wind.speed} m/s }) +
    content_tag(:li, %Q{ Wind degree: #{weather.wind.degree} º})
  end

  def snow_for(weather)
    return '' if weather.snow.nil?
    content.tag(:li, %Q{ Snow Volume: #{weather.snow.volume} })
  end

  def rain_for(weather)
    return '' if weather.rain.nil?
    content.tag(:li, %Q{ Rain Volume: #{weather.rain.volume} })
  end
end
