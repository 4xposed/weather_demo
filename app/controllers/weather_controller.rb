class WeatherController < ApplicationController
  def index
    redirect_to controller: 'weather', action: 'show', country: 'germany', locality: 'berlin'
  end

  def show
    request = Weather::API.new(weather_params).get_weather
    if request.valid?
      @weather = request.data
    else
      flash[:error] = 'Location unknown'
      redirect_back(fallback_location: root_path)
    end
  end

  def search
    redirect_to controller: 'weather', action: 'show', country: weather_params[:country_short], locality: weather_params[:locality]
  end

  def weather_params
    params.permit(:country, :locality, :country_short)
  end
end
