class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def catch_404
    flash[:error] = "Something bad happened"
    redirect_to root_path
  end
end
