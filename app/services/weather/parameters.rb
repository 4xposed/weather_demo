require 'ostruct'

class Weather::Parameters < OpenStruct
  def location
    "#{locality},#{country}"
  end
end
