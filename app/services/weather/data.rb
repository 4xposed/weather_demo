require 'ostruct'

class Weather::Data
  def initialize(status_code, body)
    @status_code = status_code
    @body = body
  end
  attr_reader :status_code, :body

  def valid?
    status_code === 200
  end

  def data
    JSON.parse(formatted_response.to_json, object_class: OpenStruct)
  end

  def formatted_response
    {
      current: {
        name:        body["weather"][0]["main"],
        description: body["weather"][0]["description"],
        icon:        body["weather"][0]["icon"],
        temperature: body["main"]["temp"],
        humidity:    body["main"]["humidity"],
        visibility:  body["visibility"]
      },
      temperature: {
        min: body["main"]["temp_min"],
        max: body["main"]["temp_max"],
      },
      location: location,
      clouds: body["clouds"]["all"],
      rain: rain,
      snow: snow,
      wind: wind
    }
  end

  def location
    locality = body["name"]
    country = body["sys"]["country"]
    {
      locality: locality,
      country: country,
      full: %Q(#{locality}, #{country})
    }
  end

  def wind
    return unless body["wind"].present?
    {
      speed: body["wind"]["speed"],
      degree: body["wind"]["deg"]
    }
  end

  def snow
    return unless body["snow"].present?
    {
      volume: body["snow"]["3h"]
    }
  end
  def rain
    return unless body["rain"].present?
    {
      volume: body["rain"]["3h"]
    }
  end
end
