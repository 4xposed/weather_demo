require 'http'

class Weather::API
  API_KEY = ENV['OPEN_WEATHER_API_KEY'].freeze
  ENDPOINT = 'http://api.openweathermap.org/data/2.5/weather'.freeze
  UNITS = 'metric'.freeze

  def initialize(params)
    @parameters = Weather::Parameters.new(params)
    @data = Weather::Data
  end
  attr_reader :parameters, :data

  def get_weather
    Weather::Data.new(query.code, query.parse)
  end

  def query
    @query ||= HTTP.get ENDPOINT, params: request_parameters
  end

  def request_parameters
    {
      q: parameters.location,
      units: UNITS,
      appid: API_KEY
    }
  end
end
