Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'weather#index'
  get 'weather/:country/:locality', to: 'weather#show'
  post 'weather/search', to: 'weather#search'
  get "*path", to: "application#catch_404", via: :all
end
