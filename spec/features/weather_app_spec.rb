require 'rails_helper'

describe "weather", :type => :feature, js: true do
  it "loads the correct weather" do
    visit '/weather/germany/berlin'

    expect(page).to have_content('Weather for: Berlin, DE')
  end

  it "searches correctly for a location" do
    visit '/'

    find('#complete').set('London')

    within('.pac-container') do
      expect(page).to have_content('London')
      expect(page).to have_content('United Kingdom')
    end
  end

  it "using a location and clicking on 'Get weather' gets you the right weather" do
    visit '/'

    find('#complete').set('Alicante, Spain')
    sleep 0.5
    find('#complete').native.send_keys(:return)
    sleep 0.5
    click_on 'Get weather'
    expect(page).to have_content('Weather for: Alicante, ES')
  end
end
